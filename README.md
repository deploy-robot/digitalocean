# digitalocean-rs

A pure Rust DigitalOcean API binding.

## Examples

### Example blocking

It needs to have the feature "blocking" enabled.

```toml
digitalocean-rs = { version = "*", features = ["blocking"] }
```

```rust
use digitalocean_rs::DigitalOceanApi;
use digitalocean_rs::DigitalOceanError;

fn main() -> Result<(), DigitalOceanError> {
    let api = DigitalOceanApi::new("api key");

    let images = api.list_images()?;
    println!("IMAGES: {:#?}", images);

    let sizes = api.list_sizes()?;
    println!("SIZES: {:#?}", sizes);
    Ok(())
}
```

### Example async

```toml
digitalocean-rs = { version = "*" }
```

```rust
use digitalocean_rs::DigitalOceanApi;
use digitalocean_rs::DigitalOceanError;

#[async_std::main]
async fn main() -> Result<(), DigitalOceanError> {
    let api = DigitalOceanApi::new("api key");

    let images = api.list_images_async().await?;
    println!("IMAGES: {:#?}", images);

    let sizes = api.list_sizes_async().await?;
    println!("SIZES: {:#?}", sizes);

    Ok(())
}
```

## Features

* "default" - use nativetls
* "default-rustls" - use rusttls
* "blocking" - enable blocking api
* "rustls" - enable rustls for reqwest
* "nativetls" - add support for nativetls DEFAULT
* "gzip" - enable gzip in reqwest
* "brotli" - enable brotli in reqwest
* "deflate" - enable deflate in reqwest

## TODO

* [ ] Documentation
* [ ] Full api support
