use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct DigitalOceanApiError {
    pub id: String,
    pub message: String,
}