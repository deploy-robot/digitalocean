use serde::Deserialize;

use super::DigitalOceanApiMeta;

#[derive(Deserialize, Debug)]
pub struct DigitalOceanSizesRoot {
    pub sizes: Vec<DigitalOceanSize>,
    pub meta: DigitalOceanApiMeta,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanSize {
    /// Id of the size
    pub slug: String,
    /// Amount of memory in MB
    pub memory: u32,
    /// Virtual cpu count
    pub vcpus: u32,
    /// Disk size in GB
    pub disk: u32,
    /// Included data transfer in TB
    pub transfer: f32,
    pub price_monthly: f64,
    pub price_hourly: f64,
    pub regions: Vec<String>,
    pub available: bool,
    pub description: String,
}
