use crate::DigitalOceanRegion;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct DigitalOceanDropletActionRoot {
    pub action: DigitalOceanDropletAction,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanDropletAction {
    pub id: u32,
    pub status: String,
    pub atype: String,
    pub started_at: String,
    pub completed_at: String,
    pub resource_id: u32,
    pub resource_type: String,
    pub region: DigitalOceanRegion,
    pub region_slug: String,
}
