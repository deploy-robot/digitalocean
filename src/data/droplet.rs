use super::DigitalOceanApiMeta;
use crate::{DigitalOceanImage, DigitalOceanSize};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct DigitalOceanDropletRoot {
    pub droplet: DigitalOceanDroplet,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanDropletsRoot {
    pub droplets: Vec<DigitalOceanDroplet>,
    pub meta: DigitalOceanApiMeta,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanDroplet {
    pub id: u32,
    pub name: String,
    // Memory size in MB
    pub memory: u32,
    pub vcpus: u32,
    // Disk size in GB
    pub disk: u32,
    pub locked: bool,
    pub status: String,
    pub kernel: Option<String>,
    pub created_at: String,
    pub features: Vec<String>,
    pub backup_ids: Vec<String>,
    pub next_backup_window: Option<DigitalOceanBackupWindow>,
    pub snapshot_ids: Vec<String>,
    pub image: DigitalOceanImage,
    pub volume_ids: Vec<String>,
    pub size: DigitalOceanSize,
    pub size_slug: String,
    pub networks: DigitalOceanNetworks,
    pub region: DigitalOceanRegion,
    pub tags: Vec<String>,
    pub vpc_uuid: Option<String>,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanBackupWindow {
    pub start: String,
    pub end: String,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanNetworks {
    pub v4: Vec<DigitalOceanNetworkv4>,
    pub v6: Vec<DigitalOceanNetworkv6>,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanNetworkv4 {
    pub ip_address: String,
    pub netmask: String,
    pub gateway: String,
    #[serde(rename = "type")]
    pub ntype: String,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanNetworkv6 {
    pub ip_address: String,
    pub netmask: u32,
    pub gateway: String,
    #[serde(rename = "type")]
    pub ntype: String,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanRegion {
    pub name: String,
    pub slug: String,
    pub features: Vec<String>,
    pub available: bool,
    pub sizes: Vec<String>,
}
