pub mod droplet;
pub mod droplet_action;
pub mod image;
pub mod size;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct DigitalOceanApiMeta {
    pub total: u32,
}
