use serde::Deserialize;

use super::DigitalOceanApiMeta;

#[derive(Deserialize, Debug)]
pub struct DigitalOceanImagesRoot {
    pub images: Vec<DigitalOceanImage>,
    pub meta: DigitalOceanApiMeta,
}

#[derive(Deserialize, Debug)]
pub struct DigitalOceanImage {
    pub id: u32,
    pub name: String,
    pub distribution: String,
    pub slug: Option<String>,
    pub public: bool,
    pub regions: Vec<String>,
    pub created_at: String,
    #[serde(rename = "type")]
    pub itype: String,
    pub min_disk_size: u32,
    pub size_gigabytes: f32,
    pub description: String,
    pub tags: Vec<String>,
    pub status: String,
    pub error_message: Option<String>,
}
