use std::fmt::Display;
use crate::api_error::DigitalOceanApiError;

#[derive(Debug)]
pub enum DigitalOceanError {
    Reqwest(reqwest::Error),
    Api(DigitalOceanApiError),
    Json(),
}

impl From<reqwest::Error> for DigitalOceanError {
    fn from(value: reqwest::Error) -> Self {
        DigitalOceanError::Reqwest(value)
    }
}

impl Display for DigitalOceanError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            DigitalOceanError::Reqwest(err) => write!(f, "Request: {}", err),
            DigitalOceanError::Api(err) => write!(f, "Api: {} {}", err.id, err.message),
            DigitalOceanError::Json() => write!(f, "JSON"),
        }
    }
}

impl std::error::Error for DigitalOceanError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }

    fn cause(&self) -> Option<&dyn std::error::Error> {
        self.source()
    }
}