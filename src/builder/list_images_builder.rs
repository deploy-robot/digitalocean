use crate::{
    data::image::DigitalOceanImagesRoot, DigitalOceanApi, DigitalOceanError, DigitalOceanImage,
};
use serde::Serialize;

#[derive(Serialize, Debug)]
struct ListImagesConfig {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    image_type: Option<ImageType>,
    #[serde(skip_serializing_if = "Option::is_none")]
    private: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    tag_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    per_page: Option<u32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    page: Option<u32>,
}

/// Builder struct for creating droplets.
///
/// A detailed documentation can be found at <https://docs.digitalocean.com/reference/api/api-reference/#operation/images_list>
pub struct ListImagesBuilder {
    api: DigitalOceanApi,
    config: ListImagesConfig,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum ImageType {
    Application,
    Distribution,
}

impl ListImagesBuilder {
    pub fn new(api: DigitalOceanApi) -> Self {
        let instancebuilder = ListImagesBuilder {
            api,
            config: ListImagesConfig {
                image_type: None,
                private: None,
                tag_name: None,
                per_page: None,
                page: None,
            },
        };
        instancebuilder
    }

    /// Filters results based on image type which can be either application or distribution.
    pub fn image_type(mut self, image_type: ImageType) -> Self {
        self.config.image_type = Some(image_type);
        self
    }

    /// Used to filter only user images.
    pub fn private(mut self, private: bool) -> Self {
        self.config.private = Some(private);
        self
    }

    /// Used to filter images by a specific tag.
    ///
    /// Example: tag_name=base-image
    pub fn tag_name(mut self, tag_name: &str) -> Self {
        self.config.tag_name = Some(tag_name.to_string());
        self
    }

    /// Number of items returned per page
    ///
    /// Default: 20
    /// Range: [1 .. 200]
    pub fn per_page(mut self, per_page: u32) -> Self {
        self.config.per_page = Some(per_page);
        self
    }

    /// Which 'page' of paginated results to return.
    pub fn page(mut self, page: u32) -> Self {
        self.config.page = Some(page);
        self
    }

    #[cfg(feature = "blocking")]
    pub fn run(self) -> Result<Vec<DigitalOceanImage>, DigitalOceanError> {
        let images = self
            .api
            .get("https://api.digitalocean.com/v2/images", self.config)?
            .json::<DigitalOceanImagesRoot>()?
            .images;

        Ok(images)
    }

    pub async fn run_async(self) -> Result<Vec<DigitalOceanImage>, DigitalOceanError> {
        let images = self
            .api
            .get_async("https://api.digitalocean.com/v2/images", self.config)
            .await?
            .json::<DigitalOceanImagesRoot>()
            .await?
            .images;

        Ok(images)
    }
}
