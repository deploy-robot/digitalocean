use digitalocean_rs::{DigitalOceanApi, DigitalOceanError};
use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Call program with the following:");
        println!("{} DO_API_KEY", args[0]);
        std::process::exit(1);
    }

    let result = do_stuff(&args[1]);
    match result {
        Ok(_) => {
            println!("Finished sucessfully");
        }
        Err(e) => {
            println!("Error: {}", e);
        }
    }
}

fn do_stuff(api_key: &str) -> Result<(), DigitalOceanError> {
    let api = DigitalOceanApi::new(api_key);

    let images = api.list_images().run()?;
    println!("IMAGES: {:#?}", images);

    let sizes = api.list_sizes()?;
    println!("SIZES: {:#?}", sizes);

    let droplets = api.list_droplets()?;
    println!("DROPLETS: {:#?}", droplets);

    Ok(())
}
